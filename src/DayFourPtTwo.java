import java.util.*;

public class DayFourPtTwo {
    public static void main(String[] args) {
        List<String> workerSched = ReadDatFile.readFile("src/dataFiles/dayFourInput.txt");
        Collections.sort(workerSched);
        String guardID = "";
        int startOfSleep = 0;
        HashMap<String, int[]> guardToMinArray = new HashMap<>();
        Set<String> allWorkers = new HashSet<>();
        for(String s: workerSched){
            String action = s.substring(19);
            int min = Integer.valueOf(s.substring(15,17).trim());
            int[] minuteArray = new int[60];

            if(action.contains("Guard")) {
                guardID = s.substring(26,30).trim();
                allWorkers.add(guardID);
            }
            if(action.contains("falls")){
                startOfSleep = min;
            }
            if(action.contains("wakes")){
                while(startOfSleep != min) {
                    if(guardToMinArray.containsKey(guardID)){
                        minuteArray = guardToMinArray.get(guardID);
                    }
                    minuteArray[startOfSleep]++;
                    if(startOfSleep == 59){
                        startOfSleep = 0;
                    }
                    else {startOfSleep++;}
                }
                guardToMinArray.put(guardID, minuteArray);
            }
        }
        for(String st : guardToMinArray.keySet()){
            int biggy = 0;
            int minute = 0;
            int[] nums = guardToMinArray.get(st);
            for(int c = 0; c< nums.length; c++) {
                if(nums[c] > biggy) {
                    biggy =nums[c];
                    minute = c;
                }
            }
            System.out.println("On minute "+minute+"Guard " + st + " number = " + biggy);
            System.out.println(Integer.valueOf(st) * minute);
        }
        System.out.println("");
    }
}

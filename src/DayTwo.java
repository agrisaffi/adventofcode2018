import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by anthonygrisaffi on 12/1/18.
 */
public class DayTwo {
    private static List<String> readFile(String fileName) {
        List<String> dataInput = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(dataInput::add);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return dataInput;
    }

    public static void main(String[] args){
        List<String> frequencyInput = readFile("src/dataFiles/dayTwoInput.txt");
        boolean doub = false;
        boolean trip = false;
        int doubles = 0;
        int triples = 0;
        int finalNumber = 0;
        for(String s : frequencyInput) {
            HashMap<Character, Integer> mappy = new HashMap<>();
            char[] letters = s.toCharArray();
            for (char c : letters){
                if(!mappy.containsKey(c)){
                    mappy.put(c,1);
                }
                else {
                    mappy.put(c, mappy.get(c) + 1);
                }
            }
            for(Character c : mappy.keySet()){
                if(mappy.get(c) ==2) {
                    doub = true;
                }
                if(mappy.get(c) == 3) {
                    trip = true;
                }
            }
            if(doub){
                doubles++;
                doub = false;
            }
            if(trip){
                triples++;
                trip = false;
            }
        }

        //part two
        for(int i = 0;i < frequencyInput.size(); i++) {
            char[] outerLetters = frequencyInput.get(i).toCharArray();
            int missingNumbers = 0;
            for(int j = i; j < frequencyInput.size(); j++ ){
                if(j == i){ continue;}
                char[] letters = frequencyInput.get(j).toCharArray();
                for(int p = 0; p < 26; p++) {
                    if (outerLetters[p] != letters[p]) {
                        missingNumbers++;
                        if (missingNumbers > 1) {
                            break;
                        }
                    }
                }
                if(missingNumbers <= 1){
                    finalNumber++;
                    for (char c: outerLetters) {System.out.print(c); }
                    System.out.println("");
                    for (char c: letters) {System.out.print(c); }
                    System.out.println("");
                }
                missingNumbers = 0;
            }
        }
        System.out.println("checksum = "+ doubles * triples);
        System.out.println("finalNumber " + finalNumber);
    }
}

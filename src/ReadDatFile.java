import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ReadDatFile {
    public static List<String> readFile(String fileName) {
        List<String> dataInput = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(dataInput::add);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return dataInput;
    }
}

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class DayThree {


    public static void main(String[] args) {
        List<String> frequencyInput = ReadDatFile.readFile("src/dataFiles/dayThreeInput");
        int [][] fabricSheet = new int[1000][1000];
        int squareCounter = 0;

        //#5 @ 638,853: 21x20
        for(String s : frequencyInput) {
            String[] splitting = s.split("\\s+");
            String[] numb = splitting[0].split("#");
            int left = Integer.valueOf(splitting[2].substring(0,splitting[2].length()-2).split(",")[0]);
            int top = Integer.valueOf(splitting[2].substring(0,splitting[2].length()-1).split(",")[1]);
            int squareOne = Integer.valueOf(splitting[3].split("x")[0]);
            int squareTwo = Integer.valueOf(splitting[3].split("x")[1]);
            int elfNumber = Integer.valueOf(splitting[0].substring(1));
            for(int i = left; i < left + squareOne; i++){
                for(int j = top; j < top + squareTwo; j++){
                    fabricSheet[i][j]++;
                }
            }
        }
        for(int i = 0; i < fabricSheet.length; i++){
            for(int p = 0; p < fabricSheet[i].length; p++){
                if(fabricSheet[i][p] > 1){
                    squareCounter++;
                }
            }
        }

        for(int i = 0; i < fabricSheet.length; i++){
            for(int p = 0; p < fabricSheet[i].length; p++){
                System.out.print(fabricSheet[i][p]);
            }
            System.out.println("");
        }

        //part two
        for(String s : frequencyInput) {
            String[] splitting = s.split("\\s+");
            String[] numb = splitting[0].split("#");
            int left = Integer.valueOf(splitting[2].substring(0,splitting[2].length()-2).split(",")[0]);
            int top = Integer.valueOf(splitting[2].substring(0,splitting[2].length()-1).split(",")[1]);
            int squareOne = Integer.valueOf(splitting[3].split("x")[0]);
            int squareTwo = Integer.valueOf(splitting[3].split("x")[1]);
            int elfNumber = Integer.valueOf(splitting[0].substring(1));
            boolean skip =false;
            for(int i = left; i < left + squareOne; i++){
                for(int j = top; j < top + squareTwo; j++){
                    if(fabricSheet[i][j] >1){
                        skip = true;
                    }
                }
            }
            if(!skip){
                //System.out.println("WINNER = "+ elfNumber);
            }
        }
        System.out.println("Squares = "+ squareCounter);
    }
}

import java.util.*;

/**
 [1518-10-14 00:02] Guard #1231 begins shift
 [1518-10-15 00:39] wakes up
 [1518-09-16 00:59] wakes up
 [1518-04-07 00:36] falls asleep
 */
public class DayFour {
    public static void main(String[] args) {
        List<String> workerSched = ReadDatFile.readFile("src/dataFiles/dayFourInput.txt");
        Collections.sort(workerSched);
        HashMap<String, Integer> guardToAwakeTime = new HashMap<>();
        int[] minuteArray = new int[60];
        String guardID = "";
        int fellAsleepMin = 0;
        Set<String> allGuards = new HashSet<>();
        for(String s: workerSched){
            String action = s.substring(19);
            int min = Integer.valueOf(s.substring(15,17));
            if(action.contains("Guard #")){
                guardID = s.substring(26,30);
                allGuards.add(guardID);
            }
            if(action.contains("falls")){
                fellAsleepMin = min;
            }
            if(action.contains("wakes")) {
                int minCount = 0;
                while(fellAsleepMin != min){
                    minCount++;
                    if(fellAsleepMin == 60){
                        fellAsleepMin = 0;
                    }
                    fellAsleepMin++;
                }
                if(guardToAwakeTime.containsKey(guardID)){
                    guardToAwakeTime.replace(guardID, guardToAwakeTime.get(guardID)+minCount);
                }
                else {guardToAwakeTime.put(guardID, minCount);}
                fellAsleepMin = 0;
            }
        }
        int highest = 0;
        String highestGuard = "";
        for(HashMap.Entry<String, Integer> g : guardToAwakeTime.entrySet() ){
            int i = g.getValue();
            if(i > highest){
                highest = i;
                highestGuard = g.getKey();
            }
        }
        System.out.println("Guard that sleeps the most minutes = "+ highestGuard);
        boolean countMinFlag = false;

        for(String s: workerSched){
            String action = s.substring(19);
            int min = Integer.valueOf(s.substring(15,17));
            if(action.contains(highestGuard)){
                countMinFlag = true;
            }
            if(action.contains("falls") && countMinFlag) {
                fellAsleepMin = min;
            }
            if(action.contains("wakes") && countMinFlag) {
                int minCount = 0;
                while(fellAsleepMin != min){
                    minCount++;
                    minuteArray[fellAsleepMin]++;
                    if(fellAsleepMin == 60){
                        fellAsleepMin = 0;
                    }
                    fellAsleepMin++;
                }
                fellAsleepMin = 0;
                countMinFlag = false;
            }
        }
        int hi = 0;
        int index = 0;
        for(int i = 0;i < minuteArray.length; i++){
            if(minuteArray[i] > hi){
                hi = minuteArray[i];
                index = i;
            }
        }
        System.out.println("Most mins  that worker " + highestGuard + " slept is = " + index);
        System.out.println(index * Integer.valueOf(highestGuard));

    }
}

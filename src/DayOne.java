import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by anthonygrisaffi on 12/1/18.
 */
public class DayOne {

    private static List<String> readFile() {
        List<String> dataInput = new  ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get("src/dataFiles/dayOneInput.txt"))) {

            stream.forEach(dataInput::add);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return dataInput;
    }

    public static void main(String[] args){
        List<String> frequencyInput = readFile();
        int result = 0;
        HashSet dupes = new HashSet();
        boolean stopper = true;
        while (stopper){
            for(String s : frequencyInput){
                if(s.startsWith("+")){
                    int num = Integer.parseInt(s.substring(1));
                    result += num;
                }
                if(s.startsWith("-")){
                    result -= Integer.parseInt(s.substring(1));
                }
                if(!dupes.add(String.valueOf(result))){
                     stopper = false;
                    System.out.println("SOLUTION == " + result);
                    return;
                }
            }
        }
        System.out.println("The frequency is == " + result);
    }
}

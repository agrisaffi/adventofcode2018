import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DaySix {



    public static void main(String[] args) {
        List<String> distances = ReadDatFile.readFile("src/dataFiles/daySixInput.txt");
        ArrayList<Integer> xCoords = new ArrayList<>();
        ArrayList<Integer> yCoords = new ArrayList<>();
        String[][] maGrid = new String[500][500];

        for (String s : distances) {
            String[] splitStrSec = s.split(",");
            xCoords.add(Integer.valueOf(splitStrSec[0].trim()));
            yCoords.add(Integer.valueOf(splitStrSec[1].trim()));
        }

        int smallestDist = 10000;
        int ascii =97;
        int symbolCounter =0;
        char[] symbols = "abcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()_+:?<>".toCharArray();
        boolean dupFlag = false;
        for(int i = 0; i < maGrid[0].length; i++){
            for(int j = 0; j < maGrid.length; j++) {
                for(int c = 0; c < xCoords.size(); c++){
                    int manDist = Math.abs(xCoords.get(c)-i)+Math.abs(yCoords.get(c)-j);
                    if(manDist < smallestDist){
                        smallestDist = manDist;
                        symbolCounter = c;
                    }
                }
                maGrid[i][j] = String.valueOf(symbols[symbolCounter]);
                smallestDist = 10000;
                symbolCounter = 0;
            }
        }
        Set<String> badLetters = new HashSet<>();


        for(int q = 0; q < maGrid[0].length; q++){
            badLetters.add(maGrid[0][q]);
        }
        for(int r = 0; r < maGrid.length; r ++) {
            badLetters.add(maGrid[r][0]);
        }
        for(int q = 0; q < maGrid[0].length; q++){
            badLetters.add(maGrid[499][q]);
        }
        for(int r = 0; r < maGrid.length; r ++) {
            badLetters.add(maGrid[r][499]);
        }
        for(int q = 0; q < maGrid[0].length; q++) {
            for (int r = 0; r < maGrid.length; r++) {
                for(String b : badLetters) {
                    if(maGrid[q][r].equals(b)){
                        maGrid[q][r] = ".";
                        break;
                    }
                }
            }
        }

        Set<String> goodLetters = new HashSet<>();
        for(int q = 0; q < maGrid[0].length; q++){
            for(int r = 0; r < maGrid.length; r++) {
                if(!maGrid[q][r].equals(".")){
                    goodLetters.add(maGrid[q][r]);
                }
            }
        }

        int num = 0;
        for(String g : goodLetters) {
            for (int q = 0; q < maGrid[0].length; q++) {
                for (int r = 0; r < maGrid.length; r++) {
                    if(maGrid[q][r].equals(g)){
                        num++;
                    }
                    //System.out.print(maGrid[q][r] + " ");
                }
                //System.out.println("");
            }
            System.out.println("The symbol "+g+"'s area is " + num);
            num = 0;
        }
    }

}

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class DaySeven {

    public static void main(String[] args) {
        List<String> directions = ReadDatFile.readFile("src/dataFiles/daySevenInput.txt");
        //Collections.sort(directions);
        HashMap<String, SevenNode<String>> listOfConnections = new HashMap<>();
        String cacheLetter = "";
        SevenNode<String> currNode = null;
        for (String s : directions) {
            String firstLetter = s.substring(5, 6);
            String secondLetter = s.substring(36, 37);

            /*if(listOfConnections.containsKey(firstLetter)) {
                SevenNode<String> nodey = listOfConnections.get(firstLetter);
                nodey.addChild(secondLetter);
            }
            else {
                currNode = new SevenNode<>(firstLetter);
                currNode.addChild(secondLetter);
                listOfConnections.put(firstLetter,currNode);
            }*/
            if (listOfConnections.containsKey(secondLetter)) {
                SevenNode<String> nodey = listOfConnections.get(secondLetter);
                nodey.addChild(firstLetter);
            } else {
                currNode = new SevenNode<>(secondLetter);
                currNode.addChild(firstLetter);
                listOfConnections.put(secondLetter, currNode);
            }
        }

        char[] keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        ArrayList<String> keysArrayList = new ArrayList<>();
        for (char c : keys) {
            keysArrayList.add(String.valueOf(c));
        }


        StringBuilder finalString = new StringBuilder();

        int count = 0;
        while (true) {
            if (keysArrayList.isEmpty()) {
                break;
            }
            String currLetter = keysArrayList.get(count);
            SevenNode<String> theNode = listOfConnections.get(currLetter);
            if (theNode == null || theNode.isLeaf()) {
                finalString.append(keysArrayList.get(count));
                listOfConnections.remove(keysArrayList.get(count));
                keysArrayList.remove(keysArrayList.get(count));
                count = -1;
            } else {
                List<SevenNode<String>> theNodeChilds = theNode.getChildren();
                for (SevenNode s : theNodeChilds) {
                    String let = (String) s.getData();
                    if (finalString.toString().contains(String.valueOf(let))) {
                        theNode.removeChild(s);
                        count = -1;
                        break;
                    }
                }
                if (listOfConnections.isEmpty()) {
                    break;
                }
            }
            count++;
            if (count >= keysArrayList.size()) {
                count = 0;
            }
        }
        ArrayList<String> wrongAnswers = new ArrayList<>();
        wrongAnswers.add("HBDROVPWAIMZNLTXCQJEKSFGYU");
        wrongAnswers.add("HBDROPVIAWMNZLTCQJEKXSFGUY");
        wrongAnswers.add("HBDROPVAIWMNZLTCEKJQXSFGUY");
        wrongAnswers.add("EUYFGKJQXSNCWILTZMAPVORDBH");
        wrongAnswers.add("HBDROPVWIAMNZLTXCQEJSKFGYU");
        wrongAnswers.add("EUYFKXGQJNSWITCLZMAPVORDBH");
        wrongAnswers.add("HBDROVPAMZLCTIWSNJQGXKFYUE");
        wrongAnswers.add("HBDROVPAMZTLIWCNSXQJKGFYUE");
        wrongAnswers.add("YUFGSKJCEXQTLZNMAWIVPORDBH");
        wrongAnswers.add("HBDROPVIWAMNZLTQXECJKSGFUY");
        wrongAnswers.add("HBDROVPAMZTLIWCNSXQJKFGYUE");
        wrongAnswers.add("EUYGFKJQXSNCWILTZMAPVORDBH");
        wrongAnswers.add("HBDROPIVAMNWZLCJKQETSFGXUY");
        wrongAnswers.add("YUXGFSTEQKJCLZWNMAVIPORDBH");
        wrongAnswers.add("HBDROVPAMZINXWTLCSQFYKJGUE");
        if (wrongAnswers.contains(finalString.toString())) {
            System.out.println("You have a wrong answer");
        }
        System.out.println(finalString);

    }
}

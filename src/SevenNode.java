import java.util.ArrayList;
import java.util.List;

public class SevenNode<String> {
    private List<SevenNode<String>> children = new ArrayList<SevenNode<String>>();
    private SevenNode<String> parent = null;
    private String data = null;

    public SevenNode(String data) {
        this.data = data;
    }

    public SevenNode(String data, SevenNode<String> parent) {
        this.data = data;
        this.parent = parent;
    }

    public List<SevenNode<String>> getChildren() {
        return children;
    }

    public void setParent(SevenNode<String> parent) {
        //parent.addChild(this);
        this.parent = parent;
    }

    public void addChild(String data) {
        SevenNode<String> child = new SevenNode<String>(data);
        child.setParent(this);
        this.children.add(child);
    }

    public void addChild(SevenNode<String> child) {
        child.setParent(this);
        this.children.add(child);
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isRoot() {
        return (this.parent == null);
    }

    public boolean isLeaf() {
        return this.children.size() == 0;
    }

    public void removeParent() {
        this.parent = null;
    }
    public void removeChild(SevenNode data) {
        this.children.remove(data);
    }
}
import java.util.ArrayList;
import java.util.List;

public class DayFive {

    public static void main(String[] args) {
        List<String> frequencyInput = ReadDatFile.readFile("src/dataFiles/dayFiveInput.txt");
        char[] charCode = frequencyInput.get(0).toCharArray();
        ArrayList<Character> code = new ArrayList<>();
        for(char c : charCode){
            code.add(c);
        }
        ArrayList<Character> afterPoly = polyBuster(code);
        System.out.println("Final code size == " + afterPoly.size());

        removingAlpha(afterPoly);
    }
    public static ArrayList<Character> polyBuster(ArrayList<Character> code) {
        int i = code.size()/2;
        int loop = 0;
        while(true) {
            if(i >= code.size()){
                i = 1;
            }
            char curr = code.get(i);
            char prev = code.get(i-1);
            if (Character.isLowerCase(prev) && Character.isUpperCase(curr)) {
                if (Character.toLowerCase(prev) == Character.toLowerCase(curr)) {
                    //System.out.println("Removed " + prev + " and " + curr);
                    code.remove(i);
                    code.remove(i-1);
                    i = code.size()/2;
                    loop = 0;
                    continue;
                }
            } else if (Character.isUpperCase(prev) && Character.isLowerCase(curr)) {
                if (Character.toLowerCase(prev) == Character.toLowerCase(curr)) {
                    //System.out.println("Removed " + prev + " and " + curr);
                    code.remove(i);
                    code.remove(i-1);
                    i = code.size()/2;
                    loop=0;
                    continue;
                }
            }
            i++;
            loop++;
            if(loop == code.size()-1) {break;}
            //System.out.println("code size  = "+ code.size());
        }
        return code;
    }

    public static void removingAlpha(ArrayList<Character> noAlpha) {
        char[] abc = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        char[] ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

        for(int a = 0; a < abc.length; a++){
            ArrayList<Character> alphaRemoved = new ArrayList<>(noAlpha);
            for(int b = 0; b < alphaRemoved.size(); b++){
                if(alphaRemoved.get(b) == abc[a] || alphaRemoved.get(b) == ABC[a]){
                   alphaRemoved.remove(b);
                   b--;
                }
            }
            System.out.println("With " + abc[a] +  " removed");
            System.out.println(polyBuster(alphaRemoved).size());
        }
    }
}
